<?php
// Final exam
// Name - Naisargy Patel

// Database name
$product = "product.db";

// Database Connection
$db = new SQLite3($product);

// Create Table "Product" into Database if not exists 
$query = "CREATE TABLE IF NOT EXISTS Product (Id INTEGER PRIMARY KEY AUTOINCREMENT,Product_Name STRING,Cost INT)";
$db->exec($query);

?>